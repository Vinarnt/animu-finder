# Animu finder
Find streaming links for your favorite animes.

## Getting started
### Install dependencies
``npm install``

### Run dev server
``npm start``

### Build production bundle
``npm build``