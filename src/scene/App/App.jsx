import React, {Component} from 'react';
import {Provider} from 'react-redux';

import './App.scss';
import NavigationBar from "../../component/NavigationBar/NavigationBar";
import HomeContainer from "../Home/HomeContainer";
import {Route, Switch} from "react-router-dom";
import Search from "../../component/NavigationBar/Search/Search";
import AnimeContainer from "../Anime/AnimeContainer";

class App extends Component {
    render() {
        return (
            <Provider store={this.props.store}>
                <div>
                    <NavigationBar/>
                    <div id="content-container">
                        <Switch>
                            <Route exact path="/" component={HomeContainer}/>
                            <Route path="/search" component={Search}/>
                            <Route path="/anime/:id(\d+)" component={AnimeContainer}/>
                        </Switch>
                    </div>
                </div>
            </Provider>
        );
    }
}

export default App;
