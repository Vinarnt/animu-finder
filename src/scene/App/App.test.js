import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import store from "../../stores/init";
import {HashRouter as Router} from "react-router-dom";

jest.mock('../../services/api/top');

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(

        <Router>
            <App store={store}/>
        </Router>,
        div);
    ReactDOM.unmountComponentAtNode(div);
});
