import React, {Component, Fragment} from 'react';
import './Anime.scss';
import Image from "../../component/Image/Image";
import CollapsiblePanelContainer from "../../component/Parts/CollapsablePanel/CollapsiblePanelContainer";

class Anime extends Component {

    render() {
        const anime = this.props.anime;

        return (
            <div id="anime-content">
                <section id="top-section">
                    <div id="top-section-content">

                        <div id="title">
                            <h2 className="title">{anime.title}</h2>
                        </div>

                        <div id="content">
                            <div id="left-column">
                                <div id="preview">
                                    <Image id="image" src={anime.image_url} width="100%" height="100%"/>
                                    <a className="button" href={anime.link_canonical} target="_blank">See on
                                        MyAnimeList</a>
                                </div>

                                <div id="titles">
                                    <h3>Titles</h3>
                                    <ul>
                                        {anime.title_english != null && <Fragment>
                                            <li>
                                                <span className="bold">English: </span>
                                                {anime.title_english}
                                            </li>
                                        </Fragment>}

                                        {anime.title_japanese != null && <Fragment>
                                            <li>
                                                <span className="bold">Japanese: </span>
                                                {anime.title_japanese}
                                            </li>
                                        </Fragment>}

                                        {anime.title_synonyms != null && <Fragment>
                                            <li>
                                                <span className="bold">Synonyms: </span>
                                                <span className="wrap-block">
                                                {anime.title_synonyms.replace(", ", "\n")}
                                                </span>
                                            </li>
                                        </Fragment>}
                                    </ul>
                                </div>

                                <div id="information">
                                    <div>
                                        <h3>Information</h3>
                                    </div>
                                </div>
                            </div>

                            <div id="description">
                                <h3>Synopsis</h3>
                                <span>{anime.synopsis}</span><br/><br/>

                                <h3>Background</h3>
                                <span>{anime.background}</span>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="bottom-section">
                    <CollapsiblePanelContainer title="Links">

                    </CollapsiblePanelContainer>
                </section>
            </div>
        );
    }
}

export default Anime;
