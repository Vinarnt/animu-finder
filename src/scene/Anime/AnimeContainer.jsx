import React, {Component} from 'react';
import {connect} from 'react-redux';

import Anime from "./Anime";
import {Loader} from "../../component/Parts/Loader/Loader";

class AnimeContainer extends Component {

    constructor(props) {
        super(props);

        props.load(props.match.params.id);
    }

    render() {
        if(!this.props.isLoading) {
            return (
                <Anime
                    isLoading={this.props.isLoading}
                    anime={this.props.anime}
                />
            );
        } else {
            return <Loader/>
        }
    }
}

const mapState = state => ({
    anime: state.anime.anime,
    isLoading: state.anime.isLoading
});

const mapDispatch = dispatch => ({
    load: (id) => dispatch.anime.load(id)
});

export default connect(mapState, mapDispatch)(AnimeContainer);