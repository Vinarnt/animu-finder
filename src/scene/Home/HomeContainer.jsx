import React from 'react';
import {connect} from 'react-redux'

import Home from "./Home";

const HomeContainer = (props) => (
    <Home
        hasMore={props.hasMore}
        isLoading={props.isLoading}
        loadMore={props.loadMore}
        animes={props.animes}
    />);

const mapState = state => ({
    animes: state.home.animes,
    hasMore: state.home.hasMore,
    isLoading: state.home.isLoading,
    page: state.home.page
});

const mapDispatch = dispatch => ({
    loadMore: dispatch.home.loadMore
});

export default connect(mapState, mapDispatch)(HomeContainer);