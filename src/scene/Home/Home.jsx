import React, {Component} from 'react';
import './Home.scss';
import AnimeGrid from '../../component/AnimeGrid/AnimeGrid';

class Home extends Component {

    render() {
        return (
            <AnimeGrid
                hasMore={this.props.hasMore}
                isLoading={this.props.isLoading}
                loadMore={this.props.loadMore}
                animes={this.props.animes}
            />
        );
    }
}

export default Home;
