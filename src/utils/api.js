const imageBaseUrl = "https://myanimelist.cdn-dena.com/images/anime/";
const imageIdentifierRegex = new RegExp(/(\d+\/\d+).jpg/);

export const getBiggerImageUrl = (imageUrl) => {

    if (imageUrl) {

        const regResult = imageIdentifierRegex.exec(imageUrl);
        if (regResult.length > 1) {

            const imageIdentifier = regResult[1];
            return `${imageBaseUrl}${imageIdentifier}.jpg`;
        }
    }

    return null;
};