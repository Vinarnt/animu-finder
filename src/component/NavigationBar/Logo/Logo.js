import React, { Component } from 'react';
import './Logo.scss';

class Logo extends Component {
    render() {
        return (
            <span className="Logo">Animu Finder</span>
        );
    }
}

export default Logo;