import React, {Component} from 'react';
import './NavigationBar.scss';
import {NavLink} from "react-router-dom";
import Logo from "./Logo/Logo";

class NavigationBar extends Component {
    render() {
        return (
            <nav className="NavigationBar">
                <Logo/>
                <NavLink exact to="/" activeClassName="link-active">Home</NavLink>
                <NavLink to="/search" activeClassName="link-active">Search</NavLink>
            </nav>
        );
    }
}

export default NavigationBar;