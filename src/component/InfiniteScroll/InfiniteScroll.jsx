import React, { Component } from 'react';
import InfiniteScroller from 'react-infinite-scroller';
import {Loader} from "../Parts/Loader/Loader";

const loader = (<Loader />);

class InfiniteScroll extends Component {
    constructor(props) {
        super(props);

        this.handleInfiniteLoad = this.handleInfiniteLoad.bind(this);
    }

    handleInfiniteLoad() {
        const { isLoading, hasMore, children, loadMore } = this.props;

        if (!children || isLoading || !hasMore) {
            return;
        }

        loadMore();
    }

    render() {
        const { isLoading, hasMore, useWindow, threshold, children } = this.props;
        return (
            <InfiniteScroller
                initialLoad={true}
                loader={null}
                className="infinite-scroll"
                hasMore={hasMore && !isLoading}
                loadMore={this.handleInfiniteLoad}
                useWindow={useWindow}
                threshold={threshold}
            >
                {children}
                {isLoading && loader}
            </InfiniteScroller>
        );
    }
}

export default InfiniteScroll;