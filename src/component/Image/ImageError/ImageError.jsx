import React from 'react';

const ImageError = () => <div>Failed to load image</div>;

export default ImageError;