import React from 'react';
import Img from 'react-image';
import ImageError from './ImageError/ImageError';
import {ImageLoader} from "../Parts/ImageLoader/ImageLoader";

const Image = (props) =>
    <Img {...props}
         loader={<ImageLoader/>}
         unloader={<ImageError/>}
    />;

export default Image;