import React, {Component} from "react";
import PropTypes from 'prop-types';
import InfiniteScroll from '../InfiniteScroll/InfiniteScroll';
import './AnimeGrid.scss'
import Cell from './Cell/Cell';

class AnimeGrid extends Component {

    render() {
        const items = [];
        this.props.animes.forEach((anime, i) => {
            items.push(
                <Cell key={anime.mal_id} anime={anime}/>
            );
        });

        return (
            <div id="anime-grid">
                <InfiniteScroll
                    loadMore={this.props.loadMore}
                    hasMore={this.props.hasMore}
                    isLoading={this.props.isLoading}
                    useWindow={true}
                    threshold={50}
                >
                    <div id="anime-grid-items">{items}</div>
                </InfiniteScroll>
            </div>
        );
    }
}

AnimeGrid.propTypes = {

    loadMore: PropTypes.func.isRequired,
    hasMore: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired
};

export default AnimeGrid;