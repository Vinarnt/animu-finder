import React, {Component} from "react";
import PropTypes from 'prop-types';

import "./Cell.scss";
import Image from "../../Image/Image";
import {Link} from "react-router-dom";


class Cell extends Component {

    render() {
        const anime = this.props.anime;

        return (
            <Link to={`/anime/${anime.mal_id}`}>
                <div className="anime-cell">
                    <div>
                        <div>
                            <Image src={anime.image_url_big} width="100%" height="100%"/>
                        </div>
                        <div className="title">{anime.title}</div>
                    </div>
                </div>
            </Link>
        );
    }
}

Cell.propTypes = {
    anime: PropTypes.object.isRequired
};

export default Cell