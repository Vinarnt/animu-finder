import React, {Component} from 'react';
import {CollapsiblePanel} from "./CollapsiblePanel";

export default class CollapsiblePanelContainer extends Component {

    constructor(props) {
        super(props);

        this.state = {
            collapsed: props.collapsed || true
        };

        this.handleCollapseClick = this.handleCollapseClick.bind(this);
    }

    render() {
        return <CollapsiblePanel
            {...this.props}
            collapse={this.state.collapsed}
            handleCollapseClick={this.handleCollapseClick}
        />
    }

    handleCollapseClick() {

        console.log("handle collapse");
        this.setState((oldState) => {
                return {
                    collapsed: !oldState.collapsed
                }
            }
        );
    }
}