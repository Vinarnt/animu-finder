import React from "react";
import PropTypes from 'prop-types';

import './CollapsiblePanel.scss'

export const CollapsiblePanel = (props) =>
    <div id="collapsible-wrapper">
        <div id="collapsible-header" onClick={props.handleCollapseClick}>
            {
                props.collapse
                ? <span>▲▲▲</span>
                : <span>▼▼▼</span>
            }
            <span className="collapsible-heading">{props.title}</span>
        </div>
        <div id="collapsible-content" className={props.collapse ? "collapsed" : ""}>
            {props.children}
        </div>
    </div>;

CollapsiblePanel.propTypes = {
    collapse: PropTypes.bool,
    title: PropTypes.string
};

CollapsiblePanel.defaultProps = {
    collapse: true,
    title: ''
};