import React from "react";
import './Loader.scss'

import Spinner from 'react-spinkit';

export const Loader = (props) => <Spinner className="loader" name="three-bounce" fadeIn="none"/>;