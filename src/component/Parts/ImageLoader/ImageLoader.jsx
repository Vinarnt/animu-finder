import React from "react";
import "./ImageLoader.scss";

import Spinner from 'react-spinkit';

export const ImageLoader = (props) => <Spinner className="image-loader" name="ball-clip-rotate-multiple" fadeIn="none"/>;