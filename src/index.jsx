import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router} from 'react-router-dom';

import registerServiceWorker from './registerServiceWorker';

import './index.scss';
import App from './scene/App/App';

import store from "./stores/init";

ReactDOM.render(
    <Router>
        <App store={store} />
    </Router>
    , document.getElementById('root'));

registerServiceWorker();
