import {anime as animeFetch} from '../services/api/anime';

export const anime = {
    state: {
        anime: null,
        isLoading: true,
    },
    reducers: {
        setAnime(state, anime) {
            return {
                ...state,
                anime: anime
            };
        },
        startLoading(state) {
            return {
                ...state,
                isLoading: true
            };
        },
        finishLoading(state) {
            return {
                ...state,
                isLoading: false
            };
        }
    },
    effects: {
        async load(id, state) {

            this.startLoading();

            console.log(`Load anime ${id}`);

            const anime = await animeFetch.get(id);

            this.setAnime(anime);
            this.finishLoading();
        }
    }
};