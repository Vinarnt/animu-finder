import {top} from '../services/api/top';

export const home = {
    state: {
        animes: [],
        hasMore: true,
        isLoading: false,
        page: 0
    },
    reducers: {
        addPage(state, animes) {
            return {
                ...state,
                animes: state.animes.concat(animes),
                page: state.page + 1
            };
        },
        startLoading(state) {
            return {
                ...state,
                isLoading: true
            };
        },
        finishLoading(state, hasMore) {
            return {
                ...state,
                hasMore: hasMore,
                isLoading: false
            };
        }
    },
    effects: {
        async loadMore(_, state) {

            this.startLoading();

            let page = state.home.page + 1;
            let hasMore = state.home.hasMore;

            console.log(`Load page ${page}`);

            const animes = await loadFromApi(page);

            this.addPage(animes);
            this.finishLoading(hasMore);
        }
    }
};

const loadFromApi = (page) => {
    return top.anime.popular(page);
};