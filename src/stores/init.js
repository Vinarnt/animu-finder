import {init} from '@rematch/core'
import {home} from './home';
import {anime} from "./anime";

const store = init({
    models: {home, anime}
});

export default store;