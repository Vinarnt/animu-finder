import {client} from "../../services/api/api";
import {ROUTES} from "../../constants/api";
import {unescape} from "../../utils/format";

export const anime = {

    get: (id) => {
        return client.get(`/${ROUTES.ANIME}/${id}`)
            .then((response) => {

                response.data.synopsis = unescape(response.data.synopsis);
                return response.data;
            });
    }
};