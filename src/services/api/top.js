import {client} from "../../services/api/api";
import {ROUTES, SUBTYPES, TYPES} from "../../constants/api";
import {getBiggerImageUrl} from "../../utils/api";

export const top = {

    anime: {
        popular: (page = 1) => {
            return client.get(`/${ROUTES.TOP}/${TYPES.ANIME}/${page}/${SUBTYPES.BYPOPULARITY}`)
                .then((response) => {

                    const animes = response.data.top;
                    animes.forEach((anime) => {

                        anime.image_url_big = getBiggerImageUrl(anime.image_url);
                    });

                    return animes;
                });
        }
    }
};