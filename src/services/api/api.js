import localforage from 'localforage'
import memoryDriver from 'localforage-memoryStorageDriver'
import { setup } from 'axios-cache-adapter'

import {BASE_URL} from "../../constants/api";

const store = localforage.createInstance({
    driver: [
        localforage.INDEXEDDB,
        localforage.WEBSQL,
        memoryDriver
    ],
    name: 'cache'
});

export const client = setup({
    cache: {
        maxAge: 30 * 60 * 1000,
        store
    },
    baseURL: BASE_URL,
    timeout: 5000
});