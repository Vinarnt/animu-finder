export const top = {

    anime: {
        popular: (page = 1) => {
            return new Promise((resolve, reject) => {

                const items = [];

                for (let i = 1; i <= 50; i++) {

                    items.push({
                        "mal_id": (page - 1) * 50 + i,
                        "rank": 1,
                        "url": "https://myanimelist.net/anime/1535/Death_Note",
                        "image_url": "https://myanimelist.cdn-dena.com/r/100x140/images/anime/9/9453.jpg?s=c90aa06a7aeeb91285aab92508edd2a1",
                        "image_url_big": "https://myanimelist.cdn-dena.com/images/anime/9/9453.jpg",
                        "title": "Death Note",
                        "type": "TV",
                        "score": 8.67,
                        "members": 1451014,
                        "airing_start": "Oct 2006",
                        "airing_end": "Jun 2007",
                        "episodes": 37
                    });
                }

                resolve(items);
            });
        }
    }
};