export const anime = {

    get: (id) => {
        return new Promise((resolve, reject) => {

            const anime = {
                "mal_id": 1,
                "link_canonical": "https://myanimelist.net/anime/1/Cowboy_Bebop",
                "title": "Cowboy Bebop",
                "title_english": "Cowboy Bebop",
                "title_japanese": "カウボーイビバップ",
                "title_synonyms": {},
                "image_url": "https://myanimelist.cdn-dena.com/images/anime/4/19644.jpg",
                "type": "TV",
                "source": "Original",
                "episodes": 26,
                "status": "Finished Airing",
                "airing": false,
                "aired_string": "Apr 3, 1998 to Apr 24, 1999",
                "aired": {
                    "from": "1998-04-03",
                    "to": "1999-04-24"
                },
                "duration": "24 min. per ep.",
                "rating": "R - 17+ (violence & profanity)",
                "score": 8.81,
                "scored_by": 368341,
                "rank": 27,
                "popularity": 38,
                "members": 714193,
                "favorites": 39466,
                "synopsis": "In the year 2071, humanity has colonized several of the planets and moons of the solar system leaving the now uninhabitable surface of planet Earth behind. The Inter Solar System Police attempts to keep peace in the galaxy, aided in part by outlaw bounty hunters, referred to as 'Cowboys'. The ragtag team aboard the spaceship Bebop are two such individuals. Mellow and carefree Spike Spiegel is balanced by his boisterous, pragmatic partner Jet Black as the pair makes a living chasing bounties and collecting rewards. Thrown off course by the addition of new members that they meet in their travels—Ein, a genetically engineered, highly intelligent Welsh Corgi; femme fatale Faye Valentine, an enigmatic trickster with memory loss; and the strange computer whiz kid Edward Wong—the crew embarks on thrilling adventures that unravel each member&#039;s dark and mysterious past little by little. Well-balanced with high density action and light-hearted comedy, Cowboy Bebop is a space Western classic and an homage to the smooth and improvised music it is named after. [Written by MAL Rewrite]",
                "background": "When Cowboy Bebop first aired in spring of 1998 on TV Tokyo, only episodes 2, 3, 7-15, and 18 were broadcast, it was concluded with a recap special known as Yose Atsume Blues. This was due to anime censorship having increased following the big controversies over Evangelion, as a result most of the series was pulled from the air due to violent content. Satellite channel WOWOW picked up the series in the fall of that year and aired it in its entirety uncensored. Cowboy Bebop was not a ratings hit in Japan, but sold over 19,000 DVD units in the initial release run, and 81,000 overall. Protagonist Spike Spiegel won Best Male Character, and Megumi Hayashibara won Best Voice Actor for her role as Faye Valentine in the 1999 and 2000 Anime Grand Prix, respectively.Cowboy Bebop&#039;s biggest influence has been in the United States, where it premiered on Adult Swim in 2001 with many reruns since. The show&#039;s heavy Western influence struck a chord with American viewers, where it became a 'gateway drug' to anime aimed at adult audiences.",
                "premiered": "Spring 1998",
                "broadcast": "Saturdays at 01:00 (JST)",
                "related": {
                    "Adaptation": [
                        {
                            "mal_id": 173,
                            "type": "manga",
                            "url": "https://myanimelist.net/manga/173/Cowboy_Bebop",
                            "title": "Cowboy Bebop"
                        },
                        {
                            "mal_id": 174,
                            "type": "manga",
                            "url": "https://myanimelist.net/manga/174/Shooting_Star_Bebop__Cowboy_Bebop",
                            "title": "Shooting Star Bebop: Cowboy Bebop"
                        }
                    ],
                    "Side story": [
                        {
                            "mal_id": 5,
                            "type": "anime",
                            "url": "https://myanimelist.net/anime/5/Cowboy_Bebop__Tengoku_no_Tobira",
                            "title": "Cowboy Bebop: Tengoku no Tobira"
                        },
                        {
                            "mal_id": 17205,
                            "type": "anime",
                            "url": "https://myanimelist.net/anime/17205/Cowboy_Bebop__Ein_no_Natsuyasumi",
                            "title": "Cowboy Bebop: Ein no Natsuyasumi"
                        }
                    ],
                    "Summary": [
                        {
                            "mal_id": 4037,
                            "type": "anime",
                            "url": "https://myanimelist.net/anime/4037/Cowboy_Bebop__Yose_Atsume_Blues",
                            "title": "Cowboy Bebop: Yose Atsume Blues"
                        }
                    ]
                },
                "producer": [
                    {
                        "url": "https://myanimelist.net/anime/producer/23/Bandai_Visual",
                        "name": "Bandai Visual"
                    }
                ],
                "licensor": [
                    {
                        "url": "https://myanimelist.net/anime/producer/102/Funimation",
                        "name": "Funimation"
                    },
                    {
                        "url": "https://myanimelist.net/anime/producer/233/Bandai_Entertainment",
                        "name": "Bandai Entertainment"
                    }
                ],
                "studio": [
                    {
                        "url": "https://myanimelist.net/anime/producer/14/Sunrise",
                        "name": "Sunrise"
                    }
                ],
                "genre": [
                    {
                        "url": "https://myanimelist.net/anime/genre/1/Action",
                        "name": "Action"
                    },
                    {
                        "url": "https://myanimelist.net/anime/genre/2/Adventure",
                        "name": "Adventure"
                    },
                    {
                        "url": "https://myanimelist.net/anime/genre/4/Comedy",
                        "name": "Comedy"
                    },
                    {
                        "url": "https://myanimelist.net/anime/genre/8/Drama",
                        "name": "Drama"
                    },
                    {
                        "url": "https://myanimelist.net/anime/genre/24/Sci-Fi",
                        "name": "Sci-Fi"
                    },
                    {
                        "url": "https://myanimelist.net/anime/genre/29/Space",
                        "name": "Space"
                    }
                ],
                "opening_theme": [
                    "\"Tank!\" by The Seatbelts (eps 1-25)"
                ],
                "ending_theme": [
                    "#1: \"The Real Folk Blues\" by The Seatbelts feat. Mai Yamane (eps 1-12, 14-25)",
                    "#2: \"Space Lion\" by The Seatbelts (ep 13)",
                    "#3: \"Blue\" by The Seatbelts feat. Mai Yamane (ep 26)"
                ]
            };

            resolve(anime);
        });
    }
};