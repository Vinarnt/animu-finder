export const BASE_URL = "https://api.jikan.moe/";

export class ROUTES {
    static SEARCH = "search";
    static TOP = "top";
    static ANIME = "anime";
}

export class TYPES {
    static ANIME =  "anime";
}

export class SUBTYPES {
    static BYPOPULARITY = "bypopularity";
}


